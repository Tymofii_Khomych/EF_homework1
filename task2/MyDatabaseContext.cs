﻿using Microsoft.EntityFrameworkCore;

namespace task_2
{
    internal class MyDatabaseContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public MyDatabaseContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=Homework1_Task2;Trusted_Connection=True;");
        }
    }
}
