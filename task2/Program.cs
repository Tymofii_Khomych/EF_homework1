﻿namespace task_2
{
    public class Program
    {
        static void FindByIndex(int index, List<Product> list)
        {
            Console.WriteLine($"ID: {list[index].ID}");
            Console.WriteLine($"Name: {list[index].Name}");
            Console.WriteLine($"Cost: {list[index].Cost}");
            Console.WriteLine($"Description: {list[index].Description}");
            Console.WriteLine($"Quantity: {list[index].Quantity} \n");
        }

        static void FindByID(Guid Id, List<Product> list)
        {
            bool found = false;

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].ID.Equals(Id))
                {
                    Console.WriteLine($"ID: {list[i].ID}");
                    Console.WriteLine($"Name: {list[i].Name}");
                    Console.WriteLine($"Cost: {list[i].Cost}");
                    Console.WriteLine($"Description: {list[i].Description}");
                    Console.WriteLine($"Quantity: {list[i].Quantity}\n");

                    found = true;
                    break;
                }
            }

            if (!found)
            {
                Console.WriteLine($"There is no product with {Id} ID\n");
            }
        }

        static void FindByName(string name, List<Product> list)
        {
            bool found = false;

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Name == name)
                {
                    Console.WriteLine($"ID: {list[i].ID}");
                    Console.WriteLine($"Name: {list[i].Name}");
                    Console.WriteLine($"Cost: {list[i].Cost}");
                    Console.WriteLine($"Description: {list[i].Description}");
                    Console.WriteLine($"Quantity: {list[i].Quantity}\n");

                    found = true;
                }
            }

            if (!found)
            {
                Console.WriteLine($"There is no product called {name}\n");
            }
        }

        static void Main(string[] args)
        {
            List<Product> products = new List<Product>();

            for (int i = 0; i < 10; i++)
            {
                products.Add(new Product($"Product {i + 1}", (i + 1) * 10.0, $"Description {i + 1}", i + 1));
            }

            foreach (var product in products)
            {
                Console.WriteLine($"ID: {product.ID}, Name: {product.Name}, Cost: {product.Cost}, Description: {product.Description}, Quantity: {product.Quantity}");
            }

            //Виведіть на екран значення за індексом > 1, 5, 0, 7
            Console.WriteLine();
            FindByIndex(1, products);
            FindByIndex(5, products);
            FindByIndex(0, products);
            FindByIndex(7, products);

            //Знайдіть та виведіть індекси > 1, 5 за властивістю Id
            FindByID(products[1].ID, products);
            FindByID(products[5].ID, products);

            //Знайдіть та виведіть індекси > 0, 7 за властивістю Name
            FindByName(products[0].Name, products);
            FindByName(products[7].Name, products);

            //  Заповніть таким самим способом, що і в першому завдані, через контекст MyDatabaseContext вашу колекцію тими самими значеннями. 

            using (MyDatabaseContext db = new MyDatabaseContext())
            {
                db.Products.AddRange(products);
                db.SaveChanges();
            }

            using (MyDatabaseContext db = new MyDatabaseContext())
            {
                Console.WriteLine("\nList of data from database\n");

                List<Product> dbProducts = db.Products.ToList();
                foreach (var product in dbProducts)
                {
                    Console.WriteLine($"ID: {product.ID}");
                    Console.WriteLine($"Name: {product.Name}");
                    Console.WriteLine($"Cost: {product.Cost}");
                    Console.WriteLine($"Description: {product.Description}");
                    Console.WriteLine($"Quantity: {product.Quantity} \n");
                }
            }

            //Знайти та вивести > 1, 5, 0, 7 з Product/User/Order (ваш варінт) контексту за ім’ям
            using (MyDatabaseContext db = new MyDatabaseContext())
            {
                List<Product> dbProducts = db.Products.ToList();

                FindByName(products[1].Name, dbProducts);
                FindByName(products[5].Name, dbProducts);
                FindByName(products[0].Name, dbProducts);
                FindByName(products[7].Name, dbProducts);
            }
        }
    }
}