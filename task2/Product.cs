﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_2
{
    public class Product
    {
        public Guid ID { get; private set; }
        public string Name { get; private set; }
        public double Cost { get; private set; }
        public string Description { get; private set; }
        public int Quantity { get; private set; }

        public Product(string name, double cost, string description, int quantity)
        {
            ID = Guid.NewGuid();
            Name = name;
            Cost = cost;
            Description = description;
            Quantity = quantity;
        }
    }
}
