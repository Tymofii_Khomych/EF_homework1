using task_2;

namespace task3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (Homework1Task2Context db = new Homework1Task2Context())
            {
                List<Product> dbProducts = db.Products.ToList();

                dataGridView1.DataSource = dbProducts;

            }
        }
    }
}